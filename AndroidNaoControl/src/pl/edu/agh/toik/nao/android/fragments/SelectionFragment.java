package pl.edu.agh.toik.nao.android.fragments;

import pl.edu.agh.toik.nao.android.R;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class SelectionFragment extends Fragment {

	
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
				
        View rootView = inflater.inflate(R.layout.fragment_select, container, false);
        return rootView;
    	
    	
    }
}

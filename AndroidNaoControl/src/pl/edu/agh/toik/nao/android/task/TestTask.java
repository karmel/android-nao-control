package pl.edu.agh.toik.nao.android.task;

import pl.edu.agh.toik.nao.android.communication.CommunicationManager;
import pl.edu.agh.toik.nao.android.task.interfaces.TestDelegate;
import android.os.AsyncTask;

public class TestTask extends AsyncTask<Void,Void,Void> {

	private TestDelegate delegate;
	
	@Override
	protected Void doInBackground(Void... params) {
		
		boolean result = CommunicationManager.getInstance().performConnectionTest();
		
		
		if (result){
			delegate.onTestSucceed();
		}else{
			delegate.onTestFailed();
		}
		
		return null;


	}

	public void setDelegate(TestDelegate delegate) {
		this.delegate = delegate;
	}

}

package pl.edu.agh.toik.nao.android.task.interfaces;

public interface TestDelegate {

	public void onTestSucceed();
	
	public void onTestFailed();
	
}

package pl.edu.agh.toik.nao.android.task;

import pl.edu.agh.toik.nao.android.communication.CommunicationManager;
import pl.edu.agh.toik.nao.android.task.interfaces.CommandDelegate;
import pl.edu.agh.toik.nao.android.task.interfaces.TestDelegate;
import android.os.AsyncTask;

public class InvokeTask extends AsyncTask<Void,Void,Void> {

	private CommandDelegate delegate;
	
	private String module;
	private String variant;
	private String params;
	
	@Override
	protected Void doInBackground(Void... param) {
		
		String result = CommunicationManager.getInstance().invokeModule(module,variant,params);
		

		delegate.onCommandExecuted(result);
		
		return null;


	}

	public void setDelegate(CommandDelegate delegate) {
		this.delegate = delegate;
	}

	public String getModule() {
		return module;
	}

	public void setModule(String module) {
		this.module = module;
	}

	public String getVariant() {
		return variant;
	}

	public void setVariant(String variant) {
		this.variant = variant;
	}

	public String getParams() {
		return params;
	}

	public void setParams(String params) {
		this.params = params;
	}

}

package pl.edu.agh.toik.nao.android.fragments;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;



import pl.edu.agh.toik.nao.android.NaoApplication;
import pl.edu.agh.toik.nao.android.R;
import pl.edu.agh.toik.nao.android.activity.modules.BatteryModuleActivity;
import pl.edu.agh.toik.nao.android.activity.modules.HelloModuleActivity;
import pl.edu.agh.toik.nao.android.activity.modules.MotionModuleActivity;
import pl.edu.agh.toik.nao.android.activity.modules.VoiceRecognitionActivity;
import android.app.ListFragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.SimpleAdapter;
 
public class ModuleListFragment extends ListFragment {

    String[] names = new String[] {
        "Hello module",
        "Battery module",
        "Motion module",
        "Voice module",
    };
 
    int[] images = new int[]{
        R.drawable.ic_launcher,
        R.drawable.ic_launcher,
        R.drawable.ic_launcher,
        R.drawable.ic_launcher
    };
 
    String[] descriptions = new String[]{
    		"Say hello to robot",
            "Read battery state",
            "Control robot motions",
            "Send voice command to robot"
    };
 
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
 
        // Each row in the list stores country name, currency and flag
        List<HashMap<String,String>> aList = new ArrayList<HashMap<String,String>>();
 
        for(int i=0;i<4;i++){
            HashMap<String, String> hm = new HashMap<String,String>();
            hm.put("txt", "" + names[i]);
            hm.put("cur","" + descriptions[i]);
            hm.put("flag", Integer.toString(images[i]) );
            aList.add(hm);
        }
 
        // Keys used in Hashmap
        String[] from = { "flag","txt","cur" };
 
        // Ids of views in listview_layout
        int[] to = { R.id.flag,R.id.txt,R.id.cur};
 
        // Instantiating an adapter to store each items
        // R.layout.listview_layout defines the layout of each item
        SimpleAdapter adapter = new SimpleAdapter(getActivity().getBaseContext(), aList, R.layout.listview_layout, from, to);
 
        setListAdapter(adapter);

        
        return super.onCreateView(inflater, container, savedInstanceState);
    }
    
    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
 
    	switch(position){
	    	case 0:
	    		startActivityForResult(new Intent(NaoApplication.getAppContext(), HelloModuleActivity.class), 0);
	    		break;
	    	case 1:
                startActivityForResult(new Intent(NaoApplication.getAppContext(), BatteryModuleActivity.class), 0);
	    		break;
	    	case 2:
                startActivityForResult(new Intent(NaoApplication.getAppContext(), MotionModuleActivity.class), 0);
	    		break;
	    	case 3:
	    		startActivityForResult(new Intent(NaoApplication.getAppContext(), VoiceRecognitionActivity.class), 0);
	    		break;
    	}
    	
    	
    	System.out.println("aaaaaaaa");
        /** Invokes the implementation of the method onListFragmentItemClick in the hosting activity */
       // ifaceItemClickListener.onListFragmentItemClick(position);
 
    }
 
}
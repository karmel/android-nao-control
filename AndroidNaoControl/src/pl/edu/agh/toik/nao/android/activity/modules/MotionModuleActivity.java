package pl.edu.agh.toik.nao.android.activity.modules;

import pl.edu.agh.toik.nao.android.R;
import pl.edu.agh.toik.nao.android.activity.AbstractActivity;
import pl.edu.agh.toik.nao.android.communication.CommunicationManager;
import pl.edu.agh.toik.nao.android.task.InvokeTask;
import pl.edu.agh.toik.nao.android.task.interfaces.CommandDelegate;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;



public class MotionModuleActivity extends AbstractActivity implements CommandDelegate {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_moves);
        TextView label = ((TextView)findViewById(R.id.serverIP1));
        label.setText(label.getText().toString()+"  "+CommunicationManager.getInstance().getInfoString());
    }
    
    public void standButtonClicked(View v){
    	callModule("0");
    }
    
    public void sitButtonClicked(View v){
    	callModule("1");
    }
    
    public void helloButtonClicked(View v){
    	callModule("2");
    }
    
    public void danceButtonClicked(View v){
    	callModule("3");
    }
    
    private void callModule(String variant){
    	
    		InvokeTask it =new InvokeTask();
    		it.setModule("MotionModule");
    		it.setParams("");
    		it.setVariant(variant);
    		it.setDelegate(this);
    		it.execute();

    	
    }

	@Override
	public void onCommandExecuted(String result) {
		// TODO Auto-generated method stub
		
	}
}

package hello_webservice;
import javax.jws.*;

import pl.edu.agh.toik.nao.server.Application;
@WebService(portName = "HelloWSPort", serviceName = "HelloWSService", targetNamespace = "http://hello_webservice/", endpointInterface = "hello_webservice.HelloWS")


public class HelloWSImpl implements HelloWS {
	
	public String hello(String name) {
		
		Application.getApp().logMessage("Received a ping from new client device", true);
		
		return "OK";

	}
}